# Betreuung


Unser Betreuungskonzept bietet zwei "lange" [Gruppen](/gruppen) bis 15.30 und eine "kurze" [Gruppe](/gruppen) bis 14.00

Bei Fragen wenden sie sich an Frau Yvonne Emmert, von den [Rennmäusen](/gruppen#rennmaus).

{{< toc >}}

## Zeiten

Die Mittagsbetreuung findet i.d.R. an allen Schultagen von [Schulschluss](https://gslerche.musin.de/unterrichtszeiten/):

*   bis 14:00 Uhr (kurze Gruppe) bzw.
*   bis 15:30 Uhr (lange Gruppe) statt.

### Betreuungsplan


| Uhrzeit   | Kinder bis 14:00 Uhr                                                  | Kinder bis 15:30 Uhr  |
|-----------|-----------                                                            |------------           |
| 11:00     | Organisatorisches                                                     | Organisatorisches     |
| 11:20     | Komm-Phase<br />- Spaß<br />- Spielen<br />- Basteln<br />- Brotzeit  | Komm-Phase<br />- Spaß<br />- Spielen<br />- Basteln<br /><br /> |
| 13:00     | Betreuungs-Phase                                                      | Essensvorbereitung    |
| 13:15     | - Spielen<br />- Basteln                                              | Mittagessen           |
| 14:00     | Ende                                                                  | Hausaufgaben          |
| 15:00     |                                                                       | Abhol-Phase (Mo-Do)<br />- Spielen<br />- Basteln |
| 15:30     |                                                                       |  Ende                 |


Freitags machen wir __keine Hausaufgaben__ und man kann die Kinder schon ab 14 abholen, außer an [Festen](/termine/#hausaufgabenfreie-freitage).


### Abholen

Die meisten Kinder der Klassenstufe 1 und 2 werden am Ende der Mittagsbetreuung von ihren Eltern abgeholt. Je nach Entwicklungsstand des Kindes und Heimweg kann durch die Eltern entscheiden werden, dass die Kinder selbst heim gehen dürfen. 3. und 4. Klässler sind oft schon so weit das sie allein heim gehen können.  
Es ist aber immer eine individuelle Entscheidung für jedes Kind.

Unabhängig davon ob das Kind abgeholt wird oder selbst heim geht. Jedes Kind muss sich bei seinen Betreuerinnen abmelden.Dazu steht pro Gruppe eine Betreuerin im Hof mit einer Liste, auf der jedes Kind abgehakt wird.
Bitte achten sie darauf dass ihr Kind sich abgemeldet hat.



### erster und letzter Schultag

Betreuung wird nach Abfrage angeboten am:

- ersten Schultag im Schuljahr für Kinder ab der zweiten Klasse __bis 14:00 Uhr__ (bei Bedarf auch bis 15.30 Uhr).
- letzten Schultag im Schuljahr bei Bedarf ab Schulschluss bis __14:00 Uhr__


## Krankheit

Bitte informieren Sie das jeweilige Betreuungsteam frühzeitig, wenn ihr Kind, z.B. wegen Krankheit, die Mittagsbetreuung einmal nicht besuchen kann (entschuldigtes Fehlen). Ein Anruf oder eine SMS an die unter den [Gruppen](/gruppen) angeführte Rufnummer genügt. 

**Die Schule benachrichtigt die Mittagsbetreuung nicht!**


## Abwesenheiten

Regelmäßige Termine während der Betreuungszeit (z.B. Sport oder Musik) sind mit dem Organisations-Team per Email abzustimmen (Email an mittagsbetreuung@grundschule-feldmoching.de).
Eine Zustimmung hierzu kann grundsätzlich für maximal einen Tag pro Woche erfolgen und wird per Email mitgeteilt.  
Weitere Fehltage sind möglichst zu vermeiden.
Bei einer Häufung an Fehltagen behält sich die Elterninitiative Mittagsbetreuung vor, den Platz anderweitig zu vergeben.

## Essen

Für die Kinder der langen Gruppe wird ein Mittagessen angeboten, für das derzeit € 85,- monatlich mit dem [Elternbeitrag](/anmeldung-kosten-kundigung) vom Konto eingezogen werden.
Das Mittagessen bekommen wir aktuell von [Kindermenü König AG](https://www.kindermenue-koenig.de).

Für die „kurzen Kinder“ in der Villa Kunterbunt gilt: Wenn die Eltern für Verpflegung sorgen, wird diese gerne von den Betreuern angerichtet und verteilt. Bitte im Interesse aller Kinder gesunde Brotzeiten mitbringen; möglichst wenig Süßigkeiten! Z.B. Obst, Rohkost, Müsli, Butterbrezen, belegte Semmeln, Getränke, …

In der Villa Kunterbunt bringen alle Eltern reihum einmal eine Wochenportion Obst und Gemüse mit.
Die Betreuerinnen kommen auf alle Eltern einzeln zu.


## Mitbringen

Die Kinder benötigen extra Hausschuhe für die Mittagsbetreuung. Es können nicht die Hausschuhe aus der Grundschule genutzt werden.

Im Winter ist es wichtig das die Kinder auch schneetaugliche Kleidung (Anorak, Schneehose) an haben. Entweder kommen die Kinder in schneetauglicher Kleidung in die Schule oder diese werden in der Mittagsbetreuung deponiert.

Es ist immer sinnvoll die Kleidung der Kinder zu beschriften.


## Aufsicht

Wichtige Angaben die Ihr Kind betreffen, z.B.  ihre Kontaktdaten, Betreuungszeiten, Regeln zur Abholung und Unverträglichkeiten, teilen Sie den Betreuerinnen mittels des sog. Abholbogens mit.


## Elterngespräche

Grundsätzlich sind kurze Tür-und-Angel-Gespräche beim [Abholen](#abholen) möglich. Aber wir können auch gern ein ausführliches Elterngespräche vereinbaren.
