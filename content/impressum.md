## Verantwortlich

Manfred Emmert
Bevollmächtigter der Elterninitiative  
Grashofstraße 16a  
80995 München

Tel: [089 / 312 32 61 6](tel:+498931232616)


## Technisch

Inhalte werden auf [gitlab.com/mittagsbetreuung-feldmoching/mittagsbetreuung-feldmoching.de](https://gitlab.com/mittagsbetreuung-feldmoching/mittagsbetreuung-feldmoching.de) verwaltet und bei [Strato](https://www.strato.de) gehostet.

Änderungswünsche gern an <mitti@klml.de>.
