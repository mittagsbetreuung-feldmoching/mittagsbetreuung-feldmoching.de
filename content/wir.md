# Über uns – die Elterninitiative Mittagsbetreuung

Die Mittagsbetreuung ist eine 1997 von Eltern gegründete Elterninitiative und wird auf ehrenamtlicher Basis geleitet. Seit Bestehen der Mittagsbetreuung wird diese von der Stadt München und der Schule aktiv unterstützt. Die Betreuung ergänzt neben dem Hort der Schule das außerschulische Betreuungsangebot für die Kinder der [Grundschule an der Lerchenauer Straße 322](https://gslerche.musin.de/). Grundsätzlich gilt das Betreuungsangebot für alle Jahrgangsstufen, wobei die bis 14:00 Uhr-Betreuung sich primär an Schulkinder der 1. bis 3. Klasse richtet. Die Betreuung wird generell an allen Schultagen angeboten.

Die Finanzierung der Mittagsbetreuung ruht auf drei Säulen, bestehend aus dem Freistaat Bayern, der Stadt München und den Beiträgen der Eltern.
Wir sind Mitglied im [Kleinkindertagesstätten e.V.](https://kkt-muenchen.de)

Mit der [schriftlichen Anmeldung](/anmeldung-kosten-kundigung) zur Mittagsbetreuung werden die Eltern Mitglieder der Initiative.

Derzeit betreuen wir in unserer Elterninitiative Mittagsbetreuung mit 13 Betreuerinnen ca. 120 Kinder, verteilt auf [drei verschiedenen Gruppen](/gruppen) und Räume.

In einer Gruppe endet die [Betreuung](/betreuung) um 14 Uhr, in den beiden anderen Gruppen um 15:30 Uhr.  Die Betreuerinnen verfügen in der Regel über langjährige Erfahrung in der Kinderbetreuung und sind überwiegend schon viele Jahre für uns tätig.

Arbeitgeber ist die Elterninitiative Mittagsbetreuung.


## Organisationsteam

Das ehrenamtliche Organisations Team kümmert sich um alle Belange als Träger und Arbeitgeber.


Leitung
: Kontakt mit Schule (Rektorin, Sekretariat, Hausmeister,...)
: Kontakt mit Verantwortlichen der Stadtverwaltung (z.B. bei Umbaumaßnahmen der MB-Räume)
: Gesamtverantwortung
: Öffentlichkeitsarbeit (Artikel im Jahrbuch und örtlicher Presse)
: Bindeglied zwischen haupt- und ehrenamtlichen MitarbeiterInnen
: Telefon: [089 / 31 23 26 16](tel:+498931232616) (Herr Manfred Emmert)
 
Finanzbuchhaltung
: Bankkontenverwaltung und -verantwortung
: Buchhaltung
: Beantragung Fördergelder der [Landeshauptstadt München](https://stadt.muenchen.de/service/info/fachabteilung-4-grund-mittel-und-foerderschulen/1081581/) und [Regierung von Oberbayern](https://www.regierung.oberbayern.bayern.de/aufgaben/37202/40455/leistung/leistung_46297/index.html)
: [Gebühreneinzüge](/anmeldung-kosten-kundigung) der Elternbeiträge
: Kalkulation Kosten und Einnahmen 
: Telefon: [0176 / 24 54 72 41](tel:+4917624547241) (Frau Anja Stiebler)

Personalwesen
: Gehaltsabrechnung unserer [hauptamtlichen Betreuerinnen](/gruppen/)
: Personalgespräche 
: Bewerbungs- und Einstellungsverfahren
: Ausstellungen, Kündigungen
: Entscheidung über Gratifikationen, Gehaltserhöhung usw
: per eMail: mittagsbetreuung@grundschule-feldmoching.de (Frau Kerstin Pöschl)

Platzvergabe
: [Anmeldung Kosten Kündigung](/anmeldung-kosten-kundigung) (incl. Vertragsverwaltung Eltern)
: Ansprechpartner für Eltern und Schule
: Telefon: [0152 / 51 05 43 70](tel:+4915251054370) (Frau Stefanie Konold )


Eventmanager
: Fortbildungen für die Betreuerinnen (z.B. 1. Hilfe Kurse)
: Geburtstagsaufmerksamkeiten für die Betreuerinnen
: Maßnahmen zum Teambuilding (Sommerabschluss, Weihnachtsfeier)


Koordination Catering
: In Absprache mit Betreuung und dem [Caterer](/betreuung/#essen)
: einmal jährlich oder bei Bedarf


Webseite
: technische Umsetzung der [Webseite](/impressum/#technisch)
: Änderungswünsche gern an <mitti@klml.de> (Herr Klaus Mueller).


Oder schicken Sie uns einfach eine eMail: mittagsbetreuung@grundschule-feldmoching.de
