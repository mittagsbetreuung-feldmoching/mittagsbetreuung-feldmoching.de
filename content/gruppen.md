# Unsere Gruppen

Derzeit betreuen wir mit 13 Betreuerinnen ca. 120 Kinder, verteilt auf die die zwei langen Gruppen (bis 15.30 Uhr) [Dschungelbande](#dschungel), [Rennmäuse](#rennmaus) und die kurze Gruppe [Villa Kunterbunt](#kunterbunt) (bis 14.00 Uhr).

{{< toc >}}

## Dschungelbande (bis 15.30 Uhr) {#dschungel}

![Team Dschungelbande](/dschungelbande.jpg)

Betreuerinnen:  
Oben: Katja, Mehrije. Unten: Andrea, Traudel

[0176 / 45 95 71 11](tel:+4917645957111)

## Rennmäuse (bis 15.30 Uhr) {#rennmaus .cb}

![Team Rennmäuse](/rennmause_2024.jpg)

Betreuerinnen:  
Barbara, Yvonne, Edyta, Ewa und Sermina

[0177 / 79 41 144](tel:+491777941144)

## Villa Kunterbunt (bis 14.00 Uhr) {#kunterbunt .cb}

![Team Villa Kunterbunt](/villakunterbunt.jpg)

Betreuerinnen:  
Kerstin, Pelin und Kollegin

[0177 / 79 41 145](tel:+491777941145)

<br class="cb" />
