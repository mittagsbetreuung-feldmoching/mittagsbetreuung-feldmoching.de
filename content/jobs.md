# Jobs

## Betreuung

Zur Unterstützung unserer [Betreuung](/betreuung/) suchen wir auch Interessenten für den [Bundesfreiwilligendienst](https://www.bundesfreiwilligendienst.de).



## Ehrenamt

Zur __ehrenamtlichen__ Mithilfe für das Orgateam suchen wir:

### Eventmanager (m/w/d)

Für die Organisation von:

* Fortbildungen für die Betreuerinnen (z.B. 1. Hilfe Kurse)
* Geburtstagsaufmerksamkeiten für die Betreuerinnen
* Maßnahmen zum Teambuiling (Sommerabschluss, Weihnachtsfeier)

### Koordination Catering  (m/w/d)

* In Absprache mit Yvonne, Andrea und dem [Caterer](/betreuung/#essen)
* einmal jährlich oder bei Bedarf
