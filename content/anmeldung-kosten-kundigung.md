# Anmeldung Kosten Kündigung

In allen Vertragsmodalitäten betreut Frau Stefanie Konold [0152 / 51 05 43 70](tel:+4915251054370) die Eltern.

Fragen/Anmerkungen bzgl. Gebühreneinzüge haben: Frau Anja Stiebler [0176 / 24 54 72 41](tel:+4917624547241).

{{< toc >}}

## Anmeldung

Die Anmeldung für das darauffolgende Schuljahr muss von September bis __31. März__ erfolgen.
Spätere Anmeldungen kommen auf die Warteliste.
Ein Beitritt während des laufenden Schuljahres ist nur bei entsprechend freier Kapazität möglich.
Um Ihr Kind anzumelden, übersenden Sie uns bitte folgende ausgefüllte Formulare zusammen mit __Arbeitszeit-Bestätigungen__ von den Arbeitgebern beider Eltern:

* [Anmeldung zur Mittagsbetreuung](/Anmeldung.pdf)

## Vergabe

Vergabe der Betreuungsplätze:

* Die Zusage der Plätze erfolgt nach Dringlichkeit und schriftlich bis __Mitte Juni__.
* Zwischen Oster- und Pfingstferien findet ein Elternabend für die neu angemeldeten Eltern statt. Die Einladung erfolgt schriftlich.


## Kündigung

Die Anmeldung zur Mittagsbetreuung ist für ein gesamtes Schuljahr bindend, um die Kosten für alle Eltern planbar und vertretbar zu halten.
Die Kündigungsfrist beträgt für die kurze Gruppe einen Monat zum nächsten Monatsende und für die lange Gruppe drei Monate zum Schuljahresende (31.08).
Kündigungen bedürfen keiner speziellen Form, müssen aber **schriftlich und fristgerecht** erfolgen!

Einem Kind kann aus wichtigem Grund der Mittagsbetreuungsplatz fristlos gekündigt werden, z.B.:

*   bei dreimaligem, unentschuldigten Fehlen des Kindes,
*   bei nicht oder unvollständig bezahlten Beiträgen,
*   wenn durch den Besuch des Kindes die Unversehrtheit anderer Kinder gefährdet ist,
*   wenn Kinder bzw. Eltern wiederholt und trotz Abmahnung gegen die Ordnung der Mittagsbetreuung verstoßen.

Kündigungen aufgrund besonderer Umstände während des laufenden Schuljahres werden individuell geprüft.


## Kosten

Für die Mittagsbetreuung ist ein monatlicher Elternbeitrag von:

* bis 14:00 Uhr ist einheitlich 40,- € / Monat (Teilzeitplätze 8,- € / Tag)
* bis 15:30 Uhr ist einheitlich 163,- € / Monat bestehend aus 78,- € Betreuungskosten & 85,- € für [Mittagessen](/betreuung/#essen) inkl. Getränke (pauschal)

Dieser wird über 11 Monate eingezogen.
Der Monat August ist beitragsfrei.

Von diesem Betrag erhalten die Gruppen pro Kind und Monat € 5,- als Spielgeld. Der Beitrag wird monatlich (außer im August) vom Girokonto eingezogen und ist auch dann voll zu entrichten, wenn ein Kind die Mittagsbetreuung nicht an allen Tagen besucht (auch bei Krankheit).

Derzeit wird eine Aufnahmegebühr in Höhe von € 30,- erhoben um z.B. Spiel- und Bastelmaterial zu ergänzen bzw. zu ersetzen.

### Absetzbarkeit

> Die Betreuungskosten für Ihr Kind können Sie als Sonderausgaben absetzen – immerhin bis zu 4.000 Euro pro Jahr und Kind.

[Kinderbetreuungskosten - was kann ich absetzen?](https://www.vlh.de/familie-leben/kinder/kinderbetreuungskosten-was-kann-ich-absetzen.html)

Eine Bestätigung für das Finanzamt ist nicht mehr nötig. Sollte doch eine Bestätigung von der Mittagsbetreuung ausgestellt werden müssen, berechnen wir eine Bearbeitungsgebühr von 7 €.
