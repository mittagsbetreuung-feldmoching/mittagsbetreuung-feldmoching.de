# Kontakt zur Elterninitiative Mittagsbetreuung


Sie möchten mit der Elterninitiative Mittagsbetreuung Kontakt aufnehmen…

… weil Sie bereits ein Kind bei uns haben und dieses entschuldigen bzw. etwas klären möchten wene sie ich per Telefon an die [Gruppe ihres Kindes](/gruppen)


… weil Sie organsiatorisches Anliegen haben, wenden sie sich an das [Organisationsteam](/wir/#organisationsteam) oder schicken Sie uns einfach eine eMail: mittagsbetreuung@grundschule-feldmoching.de
