# Termine 2025


## hausaufgabenfreie Freitage

In den [langen Gruppen](/gruppen/) findet von Montag bis Donnerstag [Hausaufgabenbetreuung](/betreuung/#betreuungsplan) statt, außer am Freitag.
Diese Nachmittage werden genutzt um z.B. Feste zu feiern (siehe in Klammern zu den entspr. Tagen) oder zu spielen, basteln etc.
Diese Tage sind sehr wichtig, für die Kinder genauso wie für die Betreuerinnen.
Deswegen bitten wir nochmals darum, dass Sie an diesen Tagen Ihr Kind nicht früher abholen, sondern ihm die Möglichkeit geben, dabei zu sein.
