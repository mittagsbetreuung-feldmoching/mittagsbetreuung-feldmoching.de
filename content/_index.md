# Elterninitiative Mittagsbetreuung

Wir freuen uns über Ihr Interesse an unserer Elterninitiative Mittagsbetreuung an der [Grundschule an der Lerchenauer Str. 322 in Feldmoching](http://gslerche.musin.de/). Auf den folgenden Seiten stellen wir unsere Arbeit vor, halten aktuelle Informationen bereit und möchten interessierten Eltern einen Überblick über unsere Arbeit vermitteln.

* [Über uns – allgemeine Informationen](wir)
* [Gruppen](gruppen)
* [Betreuung](betreuung)
* [Anmeldung Kosten Kündigung](/anmeldung-kosten-kundigung)
* [Jobs und ehrenamtliche Mithilfe](jobs)
* [Termine](termine)
* [Kontakt](kontakt)


Sparen Sie bitte nicht mit konstruktiver Kritik, Ideen und Anregungen zur Erweiterung unseres Informationsangebots.

Nur durch das Interesse und die aktive Mitarbeit vieler Eltern können wir ein konstruktives Miteinander der Elterninitiative zum Wohle unserer Kinder erreichen.
